#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Placeholder.h"
#include "Food.generated.h"

UENUM()
enum class EResizeType
{
	NONE,
	ADD,
	REMOVE
};

UCLASS()
class SNAKEGAME_API AFood : public APlaceholder
{
	GENERATED_BODY()
	
public:
	// VARIABLES
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Food Custom")
	class UParticleSystem* ParticleSystem;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Food Custom")
	EResizeType ResizeType = EResizeType::NONE;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Food Custom")
	float BonusSpeed = 0.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Food Custom")
	int32 Score = 0;
	
	// FUNCTIONS
	UFUNCTION(BlueprintNativeEvent, Category = "Food Custom")
	void EatFood();
	void EatFood_Implementation() {};
	
	AFood();
	
protected:
	// VARIABLES
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Food Custom", meta = (AllowPrivateAccess = "true"))
	class URotatingMovementComponent* RotatingMovementComponent;
	
	FTimerHandle TimerHandle;
	
	// FUNCTIONS
	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComponent,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);
		
	virtual void BeginPlay() override;
	virtual void Destroyed() override;
	void CreatingFood();
};

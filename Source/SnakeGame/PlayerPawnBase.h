#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// VARIABLES
	UPROPERTY(EditDefaultsOnly, Category = "Pawn Custom")
	TSubclassOf<class ASnakeBase> SnakeBaseClass;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Custom")
	bool bIsGameStarted = false;
	
	// FUNCTIONS
	UFUNCTION(BlueprintNativeEvent, Category = "Pawn Custom")
	void StartGame();
	void StartGame_Implementation();
	
	APlayerPawnBase();
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:
	// VARIABLES
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player Custom", meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* PawnCamera;
	
	UPROPERTY(BlueprintReadWrite)
	class ASnakeBase* SnakeBase;
	
	// FUNCTIONS
	UFUNCTION()
	void HandlePlayerVerticalInput(float value);
	
	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);

	virtual void BeginPlay() override;
};

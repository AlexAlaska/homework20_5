#include "SnakeGameGameModeBase.h"
#include "PlayerPawnBase.h"
#include "SnakeBase.h"
#include "Food.h"

#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

ASnakeGameGameModeBase::ASnakeGameGameModeBase()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;
	PrimaryActorTick.TickInterval = 1.0f;
	
	MaxXPos = MaxX * ElementSize;
	MaxYPos = MaxY * ElementSize;
}

void ASnakeGameGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	
	MaxObstacleStoreIndex = ObstacleStore.Num() - 1;
	if(MaxObstacleStoreIndex < 0)
		UE_LOG(LogTemp, Error, TEXT("%s: Obstacles Store Map is empty"), *GetName());
	
	GetWorld()->GetTimerManager().SetTimer(ObstacleIncreaseTimerHandle, this, &ASnakeGameGameModeBase::IncreaseMaxObstacles, IncreaseMaxObstaclesTimeout, true);
	GetWorld()->GetTimerManager().SetTimer(ObstacleLivetimeTimerHandle, this, &ASnakeGameGameModeBase::IncreaseObstaclesLivetime, ObstacleLivetimeTimeout, true);
	
	PlayerScore = 0;
}

void ASnakeGameGameModeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	SnakeLivetime -= 1.0f;
	if(SnakeLivetime < 0.0f && Snake != nullptr)
	{
		Snake->Destroy();
		Snake = nullptr;
	}
	
	for(auto& Elem : FoodStore)
	{
		if(Elem.Value > 0)
		{
			SpawnFoodAtLevel(Elem.Key);
			--Elem.Value;
			break;
		}
	}
	
	if(CurrentObstacles < MaxObstacles)
	{
		int32 RandomX = UKismetMathLibrary::RandomIntegerInRange(0, MaxX - 1);
		int32 RandomY = UKismetMathLibrary::RandomIntegerInRange(0, MaxY - 1);
		AddObstacleToQueue(RandomX * ElementSize, RandomY * ElementSize, ObstacleChance);
		
		while(!ObstaclesQueue.IsEmpty())
		{
			TPair<UClass*, FTransform> NewPair;
			ObstaclesQueue.Dequeue(NewPair);
			if(SpawnObstacleAtLevel(NewPair.Key, NewPair.Value))
				break;
		}
	}
}

void ASnakeGameGameModeBase::AddFoodToQueue(UClass* InFoodClass)
{
	++FoodStore[InFoodClass];
}

void ASnakeGameGameModeBase::AddObstacleToQueue(float PosX, float PosY,  int32 InObstacleChanse, EObstacleDirection Direction)
{
	auto It = ObstacleStore.CreateConstIterator();
	int32 RndVal = UKismetMathLibrary::RandomIntegerInRange(0, MaxObstacleStoreIndex);
	while(--RndVal >= 0)
		++It;
	float XYOffset = It->Value;

	if(XYOffset > 0.0f)
	{
		switch(Direction)
		{
		case EObstacleDirection::UP_LEFT:
			PosX += XYOffset;
			PosY -= XYOffset;
			break;
			
		case EObstacleDirection::LEFT_DOWN:
			PosX -= XYOffset;
			PosY -= XYOffset;
			break;
			
		case EObstacleDirection::DOWN_RIGHT:
			PosX -= XYOffset;
			PosY += XYOffset;
			break;
			
		case EObstacleDirection::NONE:
		case EObstacleDirection::RIGHT_UP:
			PosX += XYOffset;
			PosY += XYOffset;
			break;
		}
		
		if(PosX < 0.0f || PosX > MaxXPos || PosY < 0.0f || PosY > MaxYPos)
			return;
	}
	
	FRotator ObstacleRotation;
	RndVal = UKismetMathLibrary::RandomIntegerInRange(1, 100);
	if(RndVal <= 25)
		ObstacleRotation = FRotator(0.0f, 0.0f, 0.0f);
	else if(RndVal <= 50)
		ObstacleRotation = FRotator(0.0f, 90.0f, 0.0f);
	else if(RndVal <= 75)
		ObstacleRotation = FRotator(0.0f, 180.0f, 0.0f);
	else
		ObstacleRotation = FRotator(0.0f, -90.0f, 0.0f);
	
	FTransform ObstacleTransform(ObstacleRotation, FVector(PosX, PosY, ObstacleUndergroundLevel));
	TPair<UClass*, FTransform> NewPair;
	NewPair.Key = It->Key;
	NewPair.Value = ObstacleTransform;
	ObstaclesQueue.Enqueue(NewPair);
	
	if(InObstacleChanse > 0)
	{
		int32 NewObstacleChance = InObstacleChanse - ObstacleChanceDecrease;
		
		// Up-Left
		float NewX = PosX + ElementSize + XYOffset;
		float NewY = PosY - XYOffset;
		if(Direction != EObstacleDirection::DOWN_RIGHT && NewX <= MaxXPos && NewY >= 0.0f && UKismetMathLibrary::RandomIntegerInRange(1, 100) <= InObstacleChanse)
			AddObstacleToQueue(NewX, NewY, NewObstacleChance, EObstacleDirection::UP_LEFT);

		// Left-Down
		NewX = PosX - XYOffset;
		NewY = PosY - ElementSize - XYOffset;
		if(Direction != EObstacleDirection::RIGHT_UP && NewX >= 0.0f && NewY >= 0.0f && UKismetMathLibrary::RandomIntegerInRange(1, 100) <= InObstacleChanse)
			AddObstacleToQueue(NewX, NewY, NewObstacleChance, EObstacleDirection::LEFT_DOWN);
		
		// Down-Right
		NewX = PosX - ElementSize - XYOffset;
		NewY = PosY + XYOffset;
		if(Direction != EObstacleDirection::UP_LEFT && NewX >= 0.0f && NewY <= MaxYPos && UKismetMathLibrary::RandomIntegerInRange(1, 100) <= InObstacleChanse)
			AddObstacleToQueue(NewX, NewY, NewObstacleChance, EObstacleDirection::DOWN_RIGHT);
			
		// Right-Up
		NewX = PosX + XYOffset;
		NewY = PosY + ElementSize + XYOffset;
		if(Direction != EObstacleDirection::LEFT_DOWN && NewX <= MaxXPos && NewY <= MaxYPos && UKismetMathLibrary::RandomIntegerInRange(1, 100) <= InObstacleChanse)
			AddObstacleToQueue(NewX, NewY, NewObstacleChance, EObstacleDirection::RIGHT_UP);
	}
}

bool ASnakeGameGameModeBase::SpawnObstacleAtLevel(UClass* ObstacleClass, FTransform ObjectTransform)
{
	APlaceholder* NewObj = GetWorld()->SpawnActor<APlaceholder>(ObstacleClass, ObjectTransform);
	return IsValid(NewObj) ? true : false;
}

void ASnakeGameGameModeBase::SpawnFoodAtLevel(UClass* FoodClass)
{
	int32 RandomX = UKismetMathLibrary::RandomIntegerInRange(0, MaxX - 1);
	int32 RandomY = UKismetMathLibrary::RandomIntegerInRange(0, MaxY - 1);
	
	FVector SpawnLocation = FVector(RandomX * ElementSize, RandomY * ElementSize, -ElementSize);
	GetWorld()->SpawnActor<APlaceholder>(FoodClass, FTransform(SpawnLocation));
}

void ASnakeGameGameModeBase::AddScore(int32 InScore)
{
	PlayerScore += InScore;
	float TempLivetime = SnakeLivetime + InScore / 3.0f;
	if(TempLivetime > 100.0f)
		SnakeLivetime = 100.0f;
	else
		SnakeLivetime = TempLivetime;
}

void ASnakeGameGameModeBase::GameOver_Implementation()
{
	GetWorld()->GetTimerManager().ClearTimer(ObstacleLivetimeTimerHandle);
	GetWorld()->GetTimerManager().SetTimer(ObstacleIncreaseTimerHandle, this, &ASnakeGameGameModeBase::LoadNewLevel, 4.0f, false);
}

void ASnakeGameGameModeBase::LoadNewLevel_Implementation()
{
	GetWorld()->GetTimerManager().ClearTimer(ObstacleIncreaseTimerHandle);
	if(IsValid(PlayerPawn))
		PlayerPawn->bIsGameStarted = false;
}

int32 ASnakeGameGameModeBase::GetRandomInRange(int32 MinVal, int32 MaxVal)
{
	return UKismetMathLibrary::RandomIntegerInRange(MinVal, MaxVal);
}


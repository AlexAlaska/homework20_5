#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Placeholder.h"
#include "SnakeElementBase.generated.h"

UCLASS()
class SNAKEGAME_API ASnakeElementBase : public APlaceholder
{
	GENERATED_BODY()
	
public:
	// VARIABLES
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Element Custom")
	class ASnakeBase* SnakeOwner;
	
	// FUNCTIONS
	UFUNCTION(BlueprintNativeEvent, Category = "Element Custom")
	void SetFirstElementType();
	void SetFirstElementType_Implementation();
	
	UFUNCTION()
	void ToggleCollision();

	ASnakeElementBase();
	
protected:
	UFUNCTION()
	virtual void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComponent,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);
};

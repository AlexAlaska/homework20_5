#include "Food.h"
#include "Placeholder.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "SnakeGameGameModeBase.h"

#include "GameFramework/RotatingMovementComponent.h"
#include "Particles/ParticleSystem.h"
#include "Kismet/GameplayStatics.h"

AFood::AFood()
{
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;
	
	RotatingMovementComponent = CreateDefaultSubobject<URotatingMovementComponent>(TEXT("Rotating Movement Component"));
	RotatingMovementComponent->RotationRate = FRotator(0.0f, 90.0f, 0.0f);
	
	Priority = EPriorityType::FOOD;
}

void AFood::BeginPlay()
{
	Super::BeginPlay();
	
	if(!IsValid(ParticleSystem))
		UE_LOG(LogTemp, Error, TEXT("%s: Particle System not assigned"), *GetName());
	
	BoxCollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &AFood::HandleBeginOverlap);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &AFood::CreatingFood, 0.05f, true);
}

void AFood::CreatingFood()
{
	if(!bIsExist)
	{
		bIsExist = true;
		FVector EmitterLocation = GetActorLocation();
		EmitterLocation.Z += 10.0f;
		UGameplayStatics::SpawnEmitterAtLocation(this, ParticleSystem, EmitterLocation);
	}
	
	float ZValue = GetActorLocation().Z;
	if(ZValue < 0.0f)
		AddActorWorldOffset(FVector(0.0f, 0.0f, 2.5f));
	else
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
}

void AFood::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComponent,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	APlaceholder* OtherObject = Cast<APlaceholder>(OtherActor);
	if(IsValid(OtherObject))
	{
		if(OtherObject->Priority == EPriorityType::SNAKE)
		{
			if(bIsExist)
			{
				EatFood();
				
				ASnakeElementBase* SnakeElement = Cast<ASnakeElementBase>(OtherActor);
				if(IsValid(SnakeElement))
				{
					GameMode->AddScore(Score);
					if(BonusSpeed)
						SnakeElement->SnakeOwner->AddMovementSpeed(BonusSpeed);
					
					if(ResizeType == EResizeType::ADD)
					{
						SnakeElement->SnakeOwner->AddSnakeElement(true);
					}
					else if(ResizeType == EResizeType::REMOVE)
					{
						SnakeElement->SnakeOwner->AddSnakeElement(false);
					}
				}
			}
		}
		else if(OtherObject->Priority == EPriorityType::FOOD)
		{
			if(!bIsExist)
				return;
		}
		
		GameMode->AddFoodToQueue(GetPlaceholderClass());
		Destroy();
	}
}

void AFood::Destroyed()
{
	Super::Destroyed();
	
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
	
	if(bIsExist)
		UGameplayStatics::SpawnEmitterAtLocation(this, ParticleSystem, GetActorLocation());
}


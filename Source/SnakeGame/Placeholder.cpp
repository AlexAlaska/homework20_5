#include "Placeholder.h"
#include "SnakeGameGameModeBase.h"

#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"

APlaceholder::APlaceholder()
{
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;
	
	SceneRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Root Component"));
	RootComponent = SceneRootComponent;
	
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	MeshComponent->SetupAttachment(RootComponent);
	
	BoxCollisionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Collision Component"));
	BoxCollisionComponent->SetupAttachment(RootComponent);
}

void APlaceholder::BeginPlay()
{
	Super::BeginPlay();
	
	GameMode = Cast<ASnakeGameGameModeBase>(UGameplayStatics::GetGameMode(this));
	if(!IsValid(GameMode))
	{
		UE_LOG(LogTemp, Error, TEXT("%s: Game mode not assigned!"), *GetName());
	}
}

UClass* APlaceholder::GetPlaceholderClass() const
{
	return UGameplayStatics::GetObjectClass(this);
}
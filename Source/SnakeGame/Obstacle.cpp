#include "Obstacle.h"
#include "SnakeBase.h"
#include "SnakeGameGameModeBase.h"

AObstacle::AObstacle()
{
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;
	
	Priority = EPriorityType::OBSTACLE;
}

void AObstacle::BeginPlay()
{
	Super::BeginPlay();
	
	BoxCollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &AObstacle::HandleBeginOverlap);
	
	if(!bIsExist)
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &AObstacle::CreatingObstacle, 0.05f, true);
}

void AObstacle::CreatingObstacle()
{
	if(!bIsExist)
	{
		bIsExist = true;
		if(IsValid(GameMode))
			++GameMode->CurrentObstacles;
	}
	
	float ZValue = GetActorLocation().Z;
	if(ZValue < ZGround)
	{
		AddActorWorldOffset(FVector(0.0f, 0.0f, 2.0f));
		SceneRootComponent->AddLocalRotation(FRotator(0.0f, 5.0f, 0.0f));
	}
	else
	{
		int32 RndVal = GameMode->GetRandomInRange(0, GameMode->ObstacleBonusTime);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &AObstacle::StartDisposalObstacle, 15.0f + RndVal, false);
	}
}

void AObstacle::DisposalObstacle()
{
	float ZValue = GetActorLocation().Z;
	if(ZValue > GameMode->ObstacleUndergroundLevel)
	{
		AddActorWorldOffset(FVector(0.0f, 0.0f, -2.0f));
		SceneRootComponent->AddLocalRotation(FRotator(0.0f, -5.0f, 0.0f));
	}
	else
	{
		Destroy();
	}
}

void AObstacle::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComponent,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult)
{
	APlaceholder* OtherObject = Cast<APlaceholder>(OtherActor);
	if(IsValid(OtherObject))
		if(static_cast<int>(Priority) <= static_cast<int>(OtherObject->Priority) && !bIsExist)
			Destroy();
}

void AObstacle::Destroyed()
{
	Super::Destroyed();
	
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
	
	if(IsValid(GameMode) && bIsExist)
	{
		bIsExist = false;
		--GameMode->CurrentObstacles;
	}
}
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Placeholder.generated.h"

UENUM()
enum class EPriorityType
{
	FOOD,
	OBSTACLE,
	SNAKE
};

UCLASS()
class SNAKEGAME_API APlaceholder : public AActor
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Placeholder Custom")
	bool bIsExist = false;

	class ASnakeGameGameModeBase* GameMode;
	EPriorityType Priority;
		
	APlaceholder();
	
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Placeholder Custom", meta = (AllowPrivateAccess = "true"))
	class USceneComponent* SceneRootComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Placeholder Custom", meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* MeshComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Placeholder Custom", meta = (AllowPrivateAccess = "true"))
	UBoxComponent* BoxCollisionComponent;

	virtual void BeginPlay() override;
	UClass* GetPlaceholderClass() const;
};

#include "PlayerPawnBase.h"
#include "SnakeBase.h"
#include "SnakeGameGameModeBase.h"

#include "Engine/Engine.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "Engine/Classes/Components/InputComponent.h"

APlayerPawnBase::APlayerPawnBase()
{
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;
	
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Pawn Camera"));
	RootComponent = PawnCamera;
}

void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	
	SetActorRotation(FRotator(-90.0f, 0.0f, 0.0f));
	
	if(!SnakeBaseClass)
		UE_LOG(LogTemp, Error, TEXT("%s : SnakeBaseClass class is not valid!"), *GetName());
}

void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
	PlayerInputComponent->BindAction("Pause", IE_Pressed, this, &APlayerPawnBase::StartGame);
}

void APlayerPawnBase::StartGame_Implementation()
{
	if(!bIsGameStarted) 
	{
		FTransform SpawnPlace(FVector(550.0f, 1100.0f, 0.0f));
		SnakeBase = GetWorld()->SpawnActor<ASnakeBase>(SnakeBaseClass, FTransform());
		if(IsValid(SnakeBase))
		{
			SnakeBase->PrimaryActorTick.SetTickFunctionEnable(true);
			SnakeBase->GameMode->PrimaryActorTick.SetTickFunctionEnable(true);
			SnakeBase->GameMode->PlayerPawn = this;
		}
	}
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if(IsValid(SnakeBase))
	{
		if(value && SnakeBase->bIsMoveComplete)
		{
			SnakeBase->bIsMoveComplete = false;
			if(value > 0.0f && SnakeBase->LastMoveDirection != EMovementDirection::DOWN)
			{
				SnakeBase->LastMoveDirection = EMovementDirection::UP;
			}
			else if(value < 0.0f && SnakeBase->LastMoveDirection != EMovementDirection::UP)
			{
				SnakeBase->LastMoveDirection = EMovementDirection::DOWN;
			}
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if(IsValid(SnakeBase))
	{
		if(value && SnakeBase->bIsMoveComplete)
		{
			SnakeBase->bIsMoveComplete = false;
			if(value > 0.0f && SnakeBase->LastMoveDirection != EMovementDirection::LEFT)
			{
				SnakeBase->LastMoveDirection = EMovementDirection::RIGHT;
			}
			else if(value < 0.0f && SnakeBase->LastMoveDirection != EMovementDirection::RIGHT)
			{
				SnakeBase->LastMoveDirection = EMovementDirection::LEFT;
			}
		}
	}
}

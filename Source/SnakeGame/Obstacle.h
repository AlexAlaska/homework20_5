#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Placeholder.h"
#include "Obstacle.generated.h"

UENUM()
enum class EObstacleDirection
{
	NONE,
	UP_LEFT,
	LEFT_DOWN,
	DOWN_RIGHT,
	RIGHT_UP
};

UCLASS()
class SNAKEGAME_API AObstacle : public APlaceholder
{
	GENERATED_BODY()
	
public:	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Food Custom")
	float ZGround = 0.0f;

	AObstacle();

protected:
	FTimerHandle TimerHandle;
	
	virtual void BeginPlay() override;
	virtual void Destroyed() override;
	void CreatingObstacle();
	void DisposalObstacle();
	void StartDisposalObstacle() { GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &AObstacle::DisposalObstacle, 0.05f, true); }
	
	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComponent,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);
};

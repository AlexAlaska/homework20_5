#include "SnakeBase.h"
#include "Engine/Engine.h"
#include "SnakeElementBase.h"
#include "SnakeGameGameModeBase.h"

#include "Kismet/GameplayStatics.h"

ASnakeBase::ASnakeBase()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;
}

void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	
	GameMode = Cast<ASnakeGameGameModeBase>(UGameplayStatics::GetGameMode(this));
	if(!IsValid(GameMode))
		UE_LOG(LogTemp, Error, TEXT("%s: Game mode not assigned!"), *GetName());
	
	if(!IsValid(SnakeElementClass))
		UE_LOG(LogTemp, Error, TEXT("%s : SnakeElementClass class is not valid!"), *GetName());
	
	GameMode->Snake = this;
	SetActorTickInterval(MovementSpeed);
	CreateSnake();
}

void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Move();
}

void ASnakeBase::CreateSnake()
{
	int32 IndexX = GameMode->MaxX / 2;
	int32 IndexY = GameMode->MaxY / 2;
	
	for(int32 i = 0; i < NumElements; ++i)
	{
		FVector ElementLocation (IndexX * GameMode->ElementSize + SnakeElements.Num() * GameMode->ElementSize, IndexY * GameMode->ElementSize, -15.0f);
		FTransform ElementTransform (ElementLocation);
		ASnakeElementBase* NewElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, ElementTransform);
		NewElement->SnakeOwner = this;
		SnakeElements.Add(NewElement);
	}
	
	if(NumElements > 0)
		SnakeElements[0]->SetFirstElementType();
}

void ASnakeBase::AddSnakeElement(bool bIsPlus)
{
	if(bIsPlus)
	{
		FVector ElementLocation = SnakeElements.Last()->GetActorLocation();
		FTransform ElementTransform (ElementLocation);
		ASnakeElementBase* NewElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, ElementTransform);
		NewElement->SnakeOwner = this;
		SnakeElements.Add(NewElement);
	}
	else
	{
		int32 MaxIndex = SnakeElements.Num() - 1;
		if(MaxIndex == 2)
		{
			Destroy();
		}
		else
		{
			SnakeElements[MaxIndex]->Destroy();
			SnakeElements.RemoveAt(MaxIndex);
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	
	switch(LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X = GameMode->ElementSize;
		SnakeElements[0]->SetActorRotation(FRotator(0.0f, 0.0f, 0.0f));
		break;
		
	case EMovementDirection::DOWN:
		MovementVector.X = -GameMode->ElementSize;
		SnakeElements[0]->SetActorRotation(FRotator(0.0f, 180.0f, 0.0f));
		break;
		
	case EMovementDirection::LEFT:
		MovementVector.Y = -GameMode->ElementSize;
		SnakeElements[0]->SetActorRotation(FRotator(0.0f, -90.0f, 0.0f));
		break;
		
	case EMovementDirection::RIGHT:
		MovementVector.Y = GameMode->ElementSize;
		SnakeElements[0]->SetActorRotation(FRotator(0.0f, 90.0f, 0.0f));
		break;
	}
	
	SnakeElements[0]->ToggleCollision();
	
	for (int32 i = SnakeElements.Num() - 1; i > 0; --i)
	{
		ASnakeElementBase* CurrentElement = SnakeElements[i];
		ASnakeElementBase* NextElement = SnakeElements[i - 1];
		FVector NextLocation = NextElement->GetActorLocation();
		FRotator NextRotation = NextElement->GetActorRotation();
		
		CurrentElement->SetActorLocation(NextLocation);
		CurrentElement->SetActorRotation(NextRotation);
	}
	
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	bIsMoveComplete = true;
}

void ASnakeBase::SnakeElementOverlap(AActor* OtherActor)
{
		APlaceholder* PlaceholderObject = Cast<APlaceholder>(OtherActor);
	
		if(IsValid(PlaceholderObject))
			if(PlaceholderObject->Priority != EPriorityType::FOOD && PlaceholderObject->bIsExist)
				Destroy();
}

void ASnakeBase::AddMovementSpeed(float InSpeed)
{
	float TempSpeed = MovementSpeed + InSpeed;
	if (TempSpeed >= 0.05f && TempSpeed <= 0.5f)
	{
		FString InfoString;
		
		if(InSpeed < 0.0f)
		{
			InfoString = TEXT("Snake movement speed increased");
		}
		else
		{
			InfoString = TEXT("Snake movement speed decreased");
		}
		
		MovementSpeed = TempSpeed;
		SetActorTickInterval(MovementSpeed);
	}
}

void ASnakeBase::Destroyed()
{
	Super::Destroyed();
	
	for (ASnakeElementBase* Element : SnakeElements)
	{
		Element->Destroy();
	}
	SnakeElements.Empty();
	
	if(IsValid(GameMode))
		GameMode->GameOver();
}

#pragma once

#include "Obstacle.h"

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameGameModeBase.generated.h"

UCLASS()
class SNAKEGAME_API ASnakeGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	// VARIABLES
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Game Custom")
	class ASnakeBase* Snake;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Game Custom")
	class APlayerPawnBase* PlayerPawn;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Game Custom")
	TMap<TSubclassOf<class AFood>, int32> FoodStore;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Game Custom")
	TMap<TSubclassOf<AObstacle>, float> ObstacleStore;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Game Custom")
	int32 MaxX = 25;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Game Custom")
	int32 MaxY = 43;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Game Custom")
	float ElementSize = 50.0f;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Game Custom")
	int32 ObstacleChance = 30;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Game Custom")
	int32 ObstacleChanceDecrease = 10;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Game Custom")
	int32 ObstacleBonusTime = 60;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Game Custom")
	float IncreaseMaxObstaclesTimeout = 6.0f;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Game Custom")
	float ObstacleLivetimeTimeout = 2.0f;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Game Custom")
	float ObstacleUndergroundLevel = -66.0f;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Game Custom")
	float SnakeLivetime = 30.0f;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Game Custom")
	int32 MaxObstacles = 30;
	int32 CurrentObstacles = 0;
	
	// FUNCTIONS
	UFUNCTION(BlueprintNativeEvent, Category = "Game Custom")
	void LoadNewLevel();
	void LoadNewLevel_Implementation();
	
	UFUNCTION(BlueprintNativeEvent, Category = "Game Custom")
	void GameOver();
	void GameOver_Implementation();
	
	ASnakeGameGameModeBase();
	virtual void Tick(float DeltaTime) override;
	void AddFoodToQueue(UClass* InFoodClass);
	void AddScore(int32 InScore);
	int32 GetRandomInRange(int32 MinVal, int32 MaxVal);

protected:
	// VARIABLES
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Placeholder Custom", meta = (AllowPrivateAccess = "true"))
	int32 PlayerScore = 0;
	
	TQueue<TPair<UClass*, FTransform>> ObstaclesQueue;
	int32 MaxObstacleStoreIndex;
	float MaxXPos;
	float MaxYPos;
	FTimerHandle ObstacleIncreaseTimerHandle;
	FTimerHandle ObstacleLivetimeTimerHandle;
	
	// FUNCTIONS
	virtual void BeginPlay() override;
	void SpawnFoodAtLevel(UClass* FoodClass);
	bool SpawnObstacleAtLevel(UClass* ObstacleClass, FTransform ObjectTransform);
	void AddObstacleToQueue(float PosX, float PosY,  int32 InObstacleChanse, EObstacleDirection Direction = EObstacleDirection::NONE);
	void IncreaseMaxObstacles() { ++MaxObstacles; }
	void IncreaseObstaclesLivetime() { ++ObstacleBonusTime; }
};

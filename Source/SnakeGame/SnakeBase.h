#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
class ASnakeGameGameModeBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:
	// VARIABLES
	UPROPERTY(EditDefaultsOnly, Category = "Snake Custom")
	TSubclassOf<ASnakeElementBase> SnakeElementClass;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Snake Custom")
	float MovementSpeed = 0.2f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Snake Custom")
	int32 NumElements = 5;
	
	UPROPERTY()
	EMovementDirection LastMoveDirection = EMovementDirection::DOWN;
	
	ASnakeGameGameModeBase* GameMode;
	bool bIsMoveComplete = false;
	
	// FUNCTIONS
	UFUNCTION()
	void SnakeElementOverlap(AActor* OtherActor);
	
	ASnakeBase();
	void AddMovementSpeed(float InSpeed);
	void AddSnakeElement(bool bIsPlus = true);

protected:	
	// VARIABLES
	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;
	
	// FUNCTIONS
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void Destroyed() override;
	void CreateSnake();
	void Move();
};

#include "SnakeElementBase.h"
#include "SnakeBase.h"
#include "SnakeGameGameModeBase.h"

#include "Components/StaticMeshComponent.h"

ASnakeElementBase::ASnakeElementBase()
{
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;
	
	Priority = EPriorityType::SNAKE;
	bIsExist = true;
}

void ASnakeElementBase::SetFirstElementType_Implementation()
{
	BoxCollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
}

void ASnakeElementBase::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComponent,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	if(IsValid(SnakeOwner))
		SnakeOwner->SnakeElementOverlap(OtherActor);
}

void ASnakeElementBase::ToggleCollision()
{
	if(BoxCollisionComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
	{
		BoxCollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	else
	{
		BoxCollisionComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}